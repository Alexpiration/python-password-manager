# -*- coding: utf-8 -*-
"""
Created on Thu Sep  2 12:01:41 2021

@author: alexpiration
"""

from .user import User, user_auth, create_user_db
from .database import create_db_management
from .passwd import PassTable, PassEntrie, decrypt_pass


def app_launch():
    
    create_user_db()
    create_db_management()
    
    print("-"*30)
    print("PASSWORD MANAGER")
    print("-"*30)
    print("")
    print("1. Se connecter")
    print("2. Créer un identifiant")
    print("Q. Quitter")
    
    while True:
    
        try:    
            first_choice = eval(input("Que souhaitez vous faire ? : "), {'Q':'Q'})
            
            while first_choice != 'Q':
                
                if first_choice == 1:
                    print("Veuillez vous identifiez")
                    input_user = input("Identifiant : ")
                    
                    while not user_auth(input_user):
                        print("Veuillez saisir un identifiant correct")
                        input_user = input("Identifiant : ")
                    
                    print("")
                    print(f"Bienvenue {input_user}\n")
                    
                    print("1. Ajouter un mot de passe")
                    print("2. Consulter un mot de passe")
                    print("3. Modifier un mot de passe")
                    print("4. Supprimer un mot de passe")
                    print("5. Créer une nouvelle base de mot de passe")
                    print("Q : Quitter")
                    scnd_choice = eval(input("Que souhaitez vous faire ? : "), {'Q':'Q'})
                    
                    while scnd_choice != 'Q':
                        
                        if scnd_choice == 1:
                            table_name = input("Dans quelle base de donnée de mot de passe souhaitez-vous ajouter un mot de passe ? : ")
                            
                            # Check si la base de donnée existe
                            table = PassTable(table_name, input_user)
                            check = table.get_passtable()
                            
                            if check :
                            
                                app = input("A quelle application ce mot de passe est-il lié ? : ")
                                app_url = input("Vous pouvez indiquer l'url de l'application si vous le souhaitez : ")
                                app_pswd = input("Tapez le mot de passe à enregistrer : ")
                                confirmation = input("Veuillez confirmer le mot de passe : ")
                                while not app_pswd == confirmation:
                                    print("Les mot de passe sont différents, veuillez réessayer.")
                                    app_pswd = input("Tapez le mot de passe à enregistrer : ")
                                    confirmation = input("Veuillez confirmer le mot de passe : ")
                                    
                                username_check = input("Y a t-il un nom d'utilisateur particulier associé à ce mot de passe ? (O/N): ")
                                while username_check != 'O' and username_check != 'N':
                                    print("Veuillez répondre par O pour Oui, ou par N pour Non")
                                    username_check = input("Y a t-il un nom d'utilisateur particulier associé à ce mot de passe ? (O/N): ")
                                
                                if username_check == "O":
                                    different_username = input("Indiquez le nom d'utilisateur associé à ce mot de passe : ")
                                    new_pass = PassEntrie(table_name, different_username, app_pswd, app, app_url)
                                else:
                                    new_pass = PassEntrie(table_name, input_user, app_pswd, app, app_url)
                                    
                                new_pass.add_passwd()
                            
                            else:
                                print("Désolé, cette base de donnée n'existe pas, veuillez la créer en premier lieu\n")
                        
                        elif scnd_choice == 2:
                            table_name = input("Quelle est le nom de la base de donnée de mot de passe que vous souhaitez consulter ? : ")
                            print("")
                            
                            # Check si la base de donnée existe
                            table = PassTable(table_name, input_user)
                            check = table.get_passtable()
                            
                            if check :
                            
                                print("Voici les entrées présentes dans cette base de donnée : \n")
                                table.view_passtable()
                                
                                print("1. Par nom d'utilisateur")
                                print("2. Par application")
                                print("R. Retour au menu précédent")
                                choice = eval(input("Par quel moyen voulez-vous consulter le mot de passe ? : "), {'R': 'R'})
                                
                                while choice != "R":
                                    if choice == 1:
                                        username = input("Quel est l'email ou le nom d'utilisateur associé au mot de passe ? : ")
                                        result = table.get_passwd_by_username(username)
                                        if not bool(result):
                                            print("\nDésolé, aucun mot de passe trouvé associé à ce nom d'utilisateur\n")
                                            
                                        elif len(result) > 1:
                                            print("\nNous avons trouvés plusieurs résultats correspondant à ce nom d'utilisateur.\n")
                                            view_res = input("Voulez-vous voir toutes les applications associés à ce nom d'utilisateur ? (O/N) : ")
                                            
                                            while view_res != 'O' and view_res != 'N':
                                                print("Veuillez répondre par O pour Oui, ou par N pour Non")
                                                view_res = input("Voulez-vous voir toutes les applications associés à ce nom d'utilisateur ? (O/N) : ")
                                                
                                            if view_res == "O":
                                                print("|---------------{}---------------|\n".format(table.name))
                                                print("----------------{}--------------------------".format("-"*len(table.name)))
                                                print("| username | application | application url |")
                                                print("--------------------------------------------\n")
                                                for r in result:
                                                    username = r[2]
                                                    application = r[4]
                                                    application_url = r[5]
                                                    print("| {} | {} | {} |".format(username, application, application_url))
                                                print("----------------{}----------------\n".format("-"*len(table.name)))
                                                
                                        else:
                                            # 20/01/2022 Ajout check apres décryptage
                                            passwd = decrypt_pass(result[0][3])                                    
                                            
                                            if passwd is ValueError:
                                                print("Désolé, accès au mot de passe refusé\n")
                                            else:
                                                print(f"Mot de passe trouvé : {passwd}\n")
                                        
                                    elif choice == 2:
                                        app = input("Quel est le nom de l'application associé au mot de passe ? : ")
                                        result = table.get_passwd_by_app(app)
                                        
                                        if not bool(result):
                                            print("Désolé, aucun mot de passe trouvé associé à cette application\n")
                                            
                                        else:
                                            passwd = decrypt_pass(result[3])
                                            
                                            # 20/01/2022 Ajout check apres décryptage
                                            
                                            if passwd is ValueError:
                                                print("Désolé, accès au mot de passe refusé\n")
                                            else:
                                                print(f"Mot de passe trouvé : {passwd}\n")
                                    
                                    print("1. Par nom d'utilisateur")
                                    print("2. Par application")
                                    print("R. Retour au menu précédent")
                                    choice = eval(input("Par quel moyen voulez-vous consulter le mot de passe ? : "), {'R': 'R'})
                                    
                            else:
                                print("Désolé, cette base de donnée n'existe pas, veuillez la créer en premier lieu.\n")
                                
                        elif scnd_choice == 3:
                            
                            pass_table_lst = User(input_user).get_user_passtable()
                            
                            if not bool(pass_table_lst):
                                print("\n Désolé, cet utilisateur ne possède aucune base de donnée de mot de passe, veuillez en créer une en premier lieu.\n")
                            
                            else:
                                table_name_lst = list()
                                for pass_table in pass_table_lst:
                                    table_name = pass_table[2]
                                    table_name_lst.append(table_name)
                                    
                                if len(pass_table_lst) > 1:
                                    pass_table_dic = dict()
                                    for index, table in enumerate(table_name_lst):
                                        pass_table_dic[index] = table
                                    
                                    print("Voici la liste des bases de données de mot de passe que vous possedez : ")
                                    for key in pass_table_dic:
                                        print(f"{key}. {pass_table_dic[key]}")
                                    print("")
                                    table_choice = input("Dans quelle base de donnée se trouve le mot de passe à modifier ? : ")
                                    pass_table = PassTable(pass_table_dic[table_choice], input_user)
                                
                                else:
                                    pass_table_name = pass_table_lst[0][2]
                                    pass_table = PassTable(pass_table_name, input_user)
                                    print(f"Base de donnée de mot de passe : {pass_table_lst[0][2]}\n")
                                    
                                app_pass = input("De quelle application souhaitez-vous changer le mot de passe ? : ")
                                w_pass = pass_table.get_passwd_by_app(app_pass)
                                
                                if not bool(w_pass):
                                    print("\nDésolé, il n'y a pas d'entrée dans la base de donnée associée à l'application que vous venez de renseigner.")
                                    print("Faites une recherche en amont des applications que vous avez renseignées\n")
                                
                                else:
                                    new_pass = input("\nVeuillez entrer le nouveau mot de passe pour cette application : ")
                                    confirm_pass = input("Veuillez confirmer le nouveau mot de passe : ")
                                    
                                    while confirm_pass != new_pass:
                                        print("\nLes deux mots de passe entrés ne correspondent pas, veuillez réessayer")
                                        new_pass = input("\nVeuillez entrer le nouveau mot de passe pour cette application : ")
                                        confirm_pass = input("Veuillez confirmer le nouveau mot de passe : ")
                                        
                                    m_pass = PassEntrie(pass_table_name, input_user, w_pass[3], app_pass)
                                    m_pass.modify_pass(new_pass)
                                    
                                    
                        elif scnd_choice == 4:
                            pass_table_lst = User(input_user).get_user_passtable()
                            
                            if not bool(pass_table_lst):
                                print("\n Désolé, cet utilisateur ne possède aucune base de donnée de mot de passe, veuillez en créer une en premier lieu.\n")
                            
                            else:
                                table_name_lst = list()
                                for pass_table in pass_table_lst:
                                    table_name = pass_table[2]
                                    table_name_lst.append(table_name)
                                    
                                if len(pass_table_lst) > 1:
                                    pass_table_dic = dict()
                                    for index, table in enumerate(table_name_lst):
                                        pass_table_dic[index] = table
                                    
                                    print("Voici la liste des bases de données de mot de passe que vous possedez : ")
                                    for key in pass_table_dic:
                                        print(f"{key}. {pass_table_dic[key]}")
                                    print("")
                                    table_choice = input("Dans quelle base de donnée se trouve le mot de passe à supprimer ? : ")
                                    pass_table = PassTable(pass_table_dic[table_choice], input_user)
                                
                                else:
                                    pass_table_name = pass_table_lst[0][2]
                                    pass_table = PassTable(pass_table_name, input_user)
                                    print(f"Base de donnée de mot de passe : {pass_table_lst[0][2]}\n")
                                    
                                app_pass = input("De quelle application souhaitez-vous supprimer le mot de passe ? : ")
                                w_pass = pass_table.get_passwd_by_app(app_pass)
                                
                                if not bool(w_pass):
                                    print("\nDésolé, il n'y a pas d'entrée dans la base de donnée associée à l'application que vous venez de renseigner.")
                                    print("Faites une recherche en amont des applications que vous avez renseignées\n")
                                
                                else:
                                    decrypted_w_pass = decrypt_pass(w_pass[3])
                                    if decrypted_w_pass is ValueError:
                                        print("Désolé, accès au mot de passe refusé\n")
                                    else:
                                        confirm_pass = input("\nVeuillez confirmer la suppression en tapant le mot de passe pour cette application : ")
                                        
                                        while confirm_pass != decrypted_w_pass:
                                            print("\nLes deux mots de passe ne correspondent pas, veuillez réessayer")
                                            confirm_pass = input("\nVeuillez confirmer la suppression en tapant le mot de passe pour cette application : ")
                                            
                                        d_pass = PassEntrie(pass_table_name, w_pass[2], decrypted_w_pass, app_pass)
                                        d_pass.delete_pass()
                                       
                        elif scnd_choice == 5:
                            table_name = input("Quel nom voulez vous donner à cette nouvelle base de donnée de mot de passe ? : ")
                            new_table = PassTable(table_name, input_user)
                            new_table.create_pass_table()
                            
                        print("1. Ajouter un mot de passe")
                        print("2. Consulter un mot de passe")
                        print("3. Modifier un mot de passe")
                        print("4. Supprimer un mot de passe")
                        print("5. Créer une nouvelle base de mot de passe")
                        print("Q : Quitter")
                        scnd_choice = eval(input("Que souhaitez vous faire ? : "), {'Q':'Q'})
                        
                elif first_choice == 2:
                    username = input("Indiquez votre nom d'utilisateur : ")
                    user = User(username)
                    user.add_user()
                    
                # Back to main menu
                    
                print("-"*30)
                print("PASSWORD MANAGER")
                print("-"*30)
                print("\n")
                print("1. Se connecter")
                print("2. Créer un identifiant")
                print("Q. Quitter")
                
                first_choice = eval(input("Que souhaitez vous faire ? : "), {'Q':'Q'})

            break
                
        except NameError:
            print("Cette entrée ne fait pas partie des options, veuillez réessayer.\n")