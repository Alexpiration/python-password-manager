# -*- coding: utf-8 -*-
"""
Created on Wed Sep  1 13:31:58 2021

@author: alexpiration
"""
import os
import sqlite3

def create_db_management():
    db_dir = os.path.join(os.getcwd(), 'db')
    if not os.path.exists(db_dir):
        os.mkdir(db_dir)
    
    conn = sqlite3.connect(os.path.join(db_dir, 'main.db'))
    
    c = conn.cursor()
    
    verif_request = """SELECT name FROM sqlite_master WHERE type='table' AND tbl_name='database_management' """
    c.execute(verif_request)
    check = c.fetchall()
    if not bool(check):
        print("Creation de la table")
        
        c.execute("""CREATE TABLE database_management (
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    created_at TIMESTAMP NOT NULL,
                    name VARCHAR NOT NULL,
                    author_id INTEGER NOT NULL,
                    FOREIGN KEY (author_id) REFERENCES user (id))""")
        
        print("Table créée \n")
        conn.close()
        
    conn.close()
        
    