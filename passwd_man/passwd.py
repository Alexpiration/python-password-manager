# -*- coding: utf-8 -*-
"""
Created on Wed Sep  1 14:26:29 2021

@author: alexpiration
"""

import os
import sqlite3
from datetime import datetime
import platform
from getpass import getpass
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import padding
from cryptography.hazmat.primitives import serialization, hashes

from .user import User

class PassTable:
    def __init__(self, name, author):
        self.name = name
        self.author = author
        
    def create_pass_table(self):
        # Create db folder if not exist
        db_dir = os.path.join(os.getcwd(), 'db')
        if not os.path.exists(db_dir):
            os.mkdir(db_dir)
        
        conn = sqlite3.connect(os.path.join(db_dir, 'main.db'))
    
        c = conn.cursor()
        
        print("Creation de la table")
        
        sql_request_table = """CREATE TABLE {} (
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    created_at TIMESTAMP NOT NULL,
                    username VARCHAR NOT NULL,
                    passwd VARCHAR NOT NULL,
                    application VARCHAR NOT NULL,
                    application_url VARCHAR)""".format(self.name)
        c.execute(sql_request_table)
        
        print(f"Table {self.name} créée \n")
        
        now = datetime.now()
        sql_request_dbmanagement = """INSERT INTO database_management(created_at, name, author_id) VALUES ('{}', '{}', '{}')""".format(now.strftime("%m/%d/%Y, %H:%M:%S"), self.name, User(self.author).get_id)
        c.execute(sql_request_dbmanagement)
        
        conn.commit()
        conn.close()
        
    def get_passtable(self):
        conn = sqlite3.connect(os.path.join(os.getcwd(), 'db', 'main.db'))

        c = conn.cursor()
        
        verif_request = """SELECT name FROM sqlite_master WHERE type='table' AND tbl_name='{}'""".format(self.name)
        c.execute(verif_request)
        check = c.fetchall()
        if bool(check):
            return True
        else:
            return False
        
        
    def view_passtable(self):
        sql_request = """SELECT * FROM {} """.format(self.name)
        conn = sqlite3.connect(os.path.join(os.getcwd(), 'db', 'main.db'))
        c = conn.cursor()
        
        c.execute(sql_request)
        result = c.fetchall()
        conn.close()
        
        
        print("|---------------{}---------------|\n".format(self.name))
        print("----------------{}--------------------------".format("-"*len(self.name)))
        print("| username | application | application url |")
        print("--------------------------------------------\n")
        for r in result:
            username = r[2]
            application = r[4]
            application_url = r[5]
            print("| {} | {} | {} |".format(username, application, application_url))
        print("----------------{}--------------------\n".format("-"*len(self.name)))
            
        
    def get_passwd_by_username(self, username):
        sql_request = """SELECT * FROM {} WHERE username='{}'""".format(self.name, username)
        conn = sqlite3.connect(os.path.join(os.getcwd(), 'db', 'main.db'))
        c = conn.cursor()
        
        c.execute(sql_request)
        result = c.fetchall()
        conn.close()
        
        return result
        
    
    def get_passwd_by_app(self, app):
        sql_request = """SELECT * FROM {} WHERE application='{}'""".format(self.name, app)
        conn = sqlite3.connect(os.path.join(os.getcwd(), 'db', 'main.db'))
        c = conn.cursor()
        
        c.execute(sql_request)
        passwd = c.fetchone()
        conn.close()
        
        return passwd
    
    
class PassEntrie:
    def __init__(self, table_name, username, passwd, application, application_url=""):
        self.table_name = table_name
        self.username = username
        self.passwd = passwd
        self.application = application
        self.application_url = application_url
        
        
    def add_passwd(self):
        conn = sqlite3.connect(os.path.join(os.getcwd(), 'db', 'main.db'))
        
        c = conn.cursor()
        
        
        table_name = self.table_name
        new_passwd = self.crypt_pass(self.passwd)
        username = self.username
        app = self.application
        app_url = self.application_url
        
        print("Nouvelle entrée en cours d'ajout\n")
        
        now = datetime.now()
        
        c.execute("""INSERT INTO {}(created_at, username, passwd, application, application_url)
                  VALUES (?, ?, ?, ?, ?)""".format(table_name),(now.strftime("%m/%d/%Y, %H:%M:%S"), username, new_passwd, app, app_url))
        print("Nouvelle entrée ajoutée\n")
        
        conn.commit()
        conn.close()
        
    def get_pass_id(self):
        
        sql_request = """SELECT * FROM {} WHERE username='{}' AND application='{}' """.format(self.table_name, self.username, self.application)
        conn = sqlite3.connect(os.path.join(os.getcwd(), 'db', 'main.db'))
        c = conn.cursor()
        
        c.execute(sql_request)
        pass_id = c.fetchone()[0]
        
        return pass_id               
        
        
    def modify_pass(self, new_pass):
        crypt_new_pass = self.crypt_pass(new_pass)
        sql_request = """ UPDATE {} SET passwd=? WHERE id=? """.format(self.table_name)
        val = (crypt_new_pass, PassEntrie(self.table_name, self.username, self.passwd, self.application).get_pass_id())        
        conn = sqlite3.connect(os.path.join(os.getcwd(), 'db', 'main.db'))
        c = conn.cursor()
        c.execute(sql_request, val)
        conn.commit()
        print("\nLe mot de passe a bien été modifié")
        
        conn.close()
        
    
    def delete_pass(self):
        sql_request = """DELETE FROM {} WHERE id = {}""".format(self.table_name, PassEntrie(self.table_name, self.username, self.passwd, self.application).get_pass_id())
        conn = sqlite3.connect(os.path.join(os.getcwd(), 'db', 'main.db'))
        c = conn.cursor()
        c.execute(sql_request)
        conn.commit()
        print("\nLe mot de passe a bien été supprimé")
        
        
    def crypt_pass(self, passwd):
        passwd = passwd.encode('utf-8')
        
        os_sys = platform.system()
        if os_sys == 'Windows':
            pub_key_path = os.path.join(os.getenv('USERPROFILE'), 'pswdman_k', 'public_key.pem')
        elif os_sys == 'Linux':
            pub_key_path = os.path.join(os.getenv('HOME'), 'pswdman_k', 'public_key.pem')
        
        with open(pub_key_path, 'rb') as f:
            pub_key = serialization.load_pem_public_key(
                f.read(),
                backend=default_backend()
            )
        encrypt_pass = pub_key.encrypt(
                passwd,
                padding.OAEP(
                    mgf=padding.MGF1(algorithm=hashes.SHA256()),
                    algorithm=hashes.SHA256(),
                    label=None)
            )
        f.close()
        return encrypt_pass
    
    
def decrypt_pass(passwd):
    passphrase = bytes(getpass("Entrez le mot de passe de protection de votre base de donnée : "), 'utf-8')
    
    os_sys = platform.system()
    if os_sys == 'Windows':
        priv_key_path = os.path.join(os.getenv('USERPROFILE'), 'pswdman_k', 'private_key.pem')
    elif os_sys == 'Linux':
        priv_key_path = os.path.join(os.getenv('HOME'), 'pswdman_k', 'private_key.pem')
        
    try:
        with open(priv_key_path, 'rb') as key:
            priv_key = serialization.load_pem_private_key(
                    key.read(),
                    password=passphrase,
                    backend=default_backend()
                )
        decrypted = priv_key.decrypt(
                passwd,
                padding.OAEP(
                    mgf=padding.MGF1(algorithm=hashes.SHA256()),
                    algorithm=hashes.SHA256(),
                    label=None
                )
            )
        
        return decrypted.decode('utf-8')
    except ValueError:
        return ValueError
            
        
            