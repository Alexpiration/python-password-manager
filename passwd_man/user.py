# -*- coding: utf-8 -*-
"""
Created on Tue Aug 31 15:26:42 2021

@author: alexpiration
"""
import os
import sqlite3
from datetime import datetime
import platform
from getpass import getpass

import bcrypt
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.primitives import serialization


class User:
    
    def __init__(self, username):
        self.username = username
        
    
    @property
    def get_id(self):
        sql_request = """SELECT * FROM user WHERE username='{}'""".format(self.username)
        conn = sqlite3.connect(os.path.join(os.getcwd(), 'db', 'main.db'))
        c = conn.cursor()
        
        c.execute(sql_request)
        user_id = c.fetchone()[0]
        conn.close()
        return user_id
    
        
    def create_passwd(self, input_passwd):
        passwd = input_passwd.encode('utf-8')
        hashed = bcrypt.hashpw(passwd, bcrypt.gensalt())
        return hashed.decode('utf-8')
    
    
    def add_user(self):
        passwd = self.create_passwd(input("Entrez votre mot de passe maître : "))
        self.key_gen()
        now = datetime.now()
        sql_request = """INSERT INTO user(created_at, username, passwd)
                        VALUES ('{}','{}','{}')""".format(now.strftime("%m/%d/%Y, %H:%M:%S"), self.username, passwd)
        
        conn = sqlite3.connect(os.path.join(os.getcwd(), 'db', 'main.db'))
        c = conn.cursor()
        
        c.execute(sql_request)
        conn.commit()
        print(f"Utilisateur {self.username} bien ajouté à la base de donnée\n")
        conn.close()
        
    def get_user_passtable(self):
        user_id = User(self.username).get_id
        sql_request = """SELECT * FROM database_management WHERE author_id='{}'""".format(user_id)
        
        conn = sqlite3.connect(os.path.join(os.getcwd(), 'db', 'main.db'))
        c = conn.cursor()
        c.execute(sql_request)
        pass_table = c.fetchall()
        
        return pass_table
    
    
    def key_gen(self):
        private_key = rsa.generate_private_key(public_exponent=65537, key_size=2048, backend=default_backend())
        public_key = private_key.public_key()

        pass_phrase = input("Entrez un mot de passe pour protéger les données de vos bases de données : ")
        pass_phrase = pass_phrase.encode('utf-8')
        ser_private = private_key.private_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PrivateFormat.PKCS8,
            encryption_algorithm=serialization.BestAvailableEncryption(pass_phrase))

        ser_public = public_key.public_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PublicFormat.SubjectPublicKeyInfo)
        
        os_sys = platform.system()
        
        if os_sys == 'Windows':    
            k_path = os.path.join(os.getenv('USERPROFILE'), 'pswdman_k')
        
        elif os_sys == 'Linux':
            k_path = os.path.join(os.getenv('HOME'), 'pswdman_k')
            
        if not os.path.exists(k_path):
            os.mkdir(k_path)
        
        priv_path = os.path.join(k_path, 'private_key.pem')
        pub_path = os.path.join(k_path, 'public_key.pem')
        
        with open(priv_path, 'wb') as f:
            f.write(ser_private)

        with open(pub_path, 'wb') as f:
            f.write(ser_public)
            
    
    def __repr__(self):
        return f"{self.get_id(self.username)}, {self.username}"
    
    
def create_user_db():
    
    # Create db folder
    db_dir = os.path.join(os.getcwd(), 'db')
    if not os.path.exists(db_dir):
        os.mkdir(db_dir)
    
    conn = sqlite3.connect(os.path.join(db_dir, 'main.db'))

    c = conn.cursor()
    
    # Accès aux noms des tables d'une base de donnée Sqlite3 via sqlite_master table
    verif_request = """SELECT name FROM sqlite_master WHERE type='table' AND tbl_name='user' """
    c.execute(verif_request)
    check = c.fetchall()
    if not bool(check):
    
        # Creation de la table
        print("Creation de la table")
        
        c.execute("""CREATE TABLE user (
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    created_at TIMESTAMP NOT NULL,
                    username VARCHAR NOT NULL,
                    passwd VARCHAR NOT NULL)""")
        
        print("Table créée \n")
        conn.close()
    
    conn.close()
    
    # Hide main.db file
    # subprocess.check_call(["attrib", '+H', os.path.join(db_dir, 'main.db')])
    # Subprocess ne fonctionne pas sur tous les OS, voir une autre solution pour cacher le fichier db
    
    
def get_user(username):
    sql_request = """SELECT * FROM user WHERE username='{}'""".format(username)
    conn = sqlite3.connect(os.path.join(os.getcwd(), 'db', 'main.db'))
    c = conn.cursor()
    
    c.execute(sql_request)
    user = c.fetchone()
    conn.close()
    
    if user is not None:
        return True
    
    else:
        print("Erreur, identifiant incorrect")
        return False
    
def get_user_passwd(username):
    sql_request = """SELECT * FROM user WHERE username='{}'""".format(username)
    conn = sqlite3.connect(os.path.join(os.getcwd(), 'db', 'main.db'))
    c = conn.cursor()
    
    c.execute(sql_request)
    user_pass = c.fetchone()[3]
    conn.close()
    
    if user_pass is not None:
        return user_pass.encode('utf-8')

def user_auth(username):
    if get_user(username):
        input_pass = getpass("Entrez votre mot de passe maître : ").encode('utf-8')
        user_pass = get_user_passwd(username)
        
        if bcrypt.checkpw(input_pass, user_pass):
            return True
        
        else:
            print("Erreur, identifiants incorrect")
            return False
            

