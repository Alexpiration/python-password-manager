#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 25 22:18:18 2022

@author: alexpiration
"""

from passwd_man.app import app_launch

def main():
    app_launch()
    
if __name__ == '__main__':
    main()